# ob1-scanner
The changes discussed below are in the `scanner-cli-configuration` branch.

## Purpose
This fork of the ob1-firmware features modifications to the ob1-scanner utility (`src/ipscanner/ob1-scanner`). So far, the additions include:
* Logging to display the number of miners found by the scanner.
* `config build`, `config update`, and `config deploy` subcommands for building and managing an Obelisk cluster
* Changes to make the ob1-scanner easier to work on locally since the version in the firmware references a private repository. 

## Usage
### ob1-scanner config
The configuration subcommand `config` allows you to build, update, and deploy json configurations for all detected Obelisk units on a network.
#### config build
This will build a json configuration with a few default groups and all miners detected using the scanner tool. The groups provide you with a way to configure all of your miners in one place, with any of the data stored on the miner accessible as a templated value (for example, you can access the miner's name with `{{Name}}` in the group configuration). During the deployment phase, each miner's group configuration will be rendered for that miner and deployed via the Obelisk API.
#### config update
This will add new miners to your configuration file and warn you if miners in configuration are no longer discoverable (it will not remove them). It will also update the IP address if you aren't using static IPs (like me), allowing you to confidently deploy the configuration.
#### config deploy
This will first attempt to update the configuration to ensure the IP addresses are correct. It will also check for new miners. If any new miners are added to the configuration the configuration will not be deployed (you can just run it again, this is to ensure that the miners are given an appropriate group). This will then render and deploy the configuration for each miner via the Obelisk API.

## Local dev setup
1. Install Go
2. `git clone` this repository and `git checkout scanner-cli-configuration`.
3. Symlink (ln -s) the ob1-scanner directory (`src/ipscanner/ob1-scanner`) to your `go/src` folder.
4. Use `go get` and `go build` to acquire dependencies and build artifacts for using the tool.
